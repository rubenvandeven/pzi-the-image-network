The Image Network
====================

The image as network; sample code from [Piet Zwart Media Design](http://pzwart3.wdka.hro.nl/wiki/) / Networked Media.

Based on the text [Programming Computer Vision with Pytho](http://programmingcomputervision.com).
