from pylab import *
from numpy import *
from PCV.localdescriptors import sift
import sys, os
from PIL import Image

path = sys.argv[1]
from glob import glob
imlist = glob(os.path.join(path, "*.jpg"))
nbr_images = len(imlist)

# extract features
featlist = [imname[:-3]+'sift' for imname in imlist]
for i,imname in enumerate(imlist):
    if not os.path.exists(featlist[i]):
        sift.process_image(imname, featlist[i])
        try:
            l1, d1 = sift.read_features_from_file(featlist[i])
            figure()
            gray()
            im0 = Image.open(imname)
            im1 = array(im0.convert('L'))
            im0.close()
            sift.plot_features(im1, l1, circle=True)
            savefig(featlist[i]+".png")
            del im1
        except IndexError:
            pass
            
        #show()



matchscores = zeros((nbr_images,nbr_images))

for i in range(nbr_images):
    for j in range(i,nbr_images): # only compute upper triangle
        print 'comparing (', i,'/',nbr_images,'):', imlist[i], imlist[j]
        try:
            l1,d1 = sift.read_features_from_file(featlist[i]) 
            l2,d2 = sift.read_features_from_file(featlist[j])
            matches = sift.match_twosided(d1,d2)
            nbr_matches = sum(matches > 0)
            print 'number of matches = ', nbr_matches 
            matchscores[i,j] = nbr_matches
        except IndexError as e:
            print "Warning, error", e
# copy values
for i in range(nbr_images):
    for j in range(i+1,nbr_images): # no need to copy diagonal
        matchscores[j,i] = matchscores[i,j]

matchscores.dump(os.path.join(path, "matchscores.dump"))

